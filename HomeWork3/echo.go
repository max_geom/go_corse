package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)


// RequestInterface - получение данных из запроса
type RequestInterface interface {
	GetData(request *http.Request) map[string]string
}

// JsonEcho - структуда для работы с данными в формате json
type JsonEcho struct{}

// FormEcho - структуда для работы с данными в формате form
type FormEcho struct{}

// GetData - получение данных в формате json и приведение к одному формату
func (j JsonEcho) GetData(request *http.Request) map[string]string {
	var jsonData map[string]string
	dec := json.NewDecoder(request.Body)
	dec.Decode(&jsonData)
	fmt.Println(jsonData)
	return jsonData
}

// GetData - получение данных в формате form и приведение к одному формату
func (f FormEcho) GetData(request *http.Request) map[string]string {
	formData := make(map[string]string)
	for key, value := range request.Form {
		formData[key] = value[0]
	}
	fmt.Println(formData)
	return formData
}

func main() {
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method == "POST" {
			_ = request.ParseForm()
			jsonKwargs := JsonEcho{}
			formKwargs := FormEcho{}

			result := []RequestInterface{formKwargs, jsonKwargs}
			for _, item := range result {
				dataRequest := item.GetData(request)
				for _, value := range dataRequest {
					_, err := fmt.Fprintln(writer, value)
					if err != nil {
						log.Println(err)
					}
				}
			}
		}
	})
	log.Fatal(http.ListenAndServe(":8000", nil))
}
