package main

import (
	"fmt"
	"sync"
)


// merge - Запись результаты из нескольких каналов в один
func merge(ch ...<-chan int) <-chan int{
	var wGroup sync.WaitGroup
	out := make(chan int)

	output := func(ch <-chan int) {
		defer wGroup.Done()
		for item := range ch {
			out <- item
		}
	}

	wGroup.Add(len(ch))
	for _, c := range ch {
		go output(c)
	}

	go func() {
		wGroup.Wait()
		close(out)
	}()
	return out
}

// gen - создаём канал и записываем в него числа
func gen(nums ...int) <-chan int {
	out := make(chan int, len(nums))
	for _, n := range nums {
		out <- n
	}
	close(out)
	return out
}

// sq - извлекаем из канала в параметре элементы и делаем квадраты этих чисел. Возвращаем канал квадратов
func sq(in <-chan int) <-chan int {
	out := make(chan int)
	go func() {
		for n := range in {
			out <- n * n
		}
		close(out)
	}()
	return out
}


/*
	Тестовая программа, чтобы понять как работает WaitGroup.
	Реализация слияния 2х каналов в один.
	Код был взят с официального сайта
 */
func main() {
	in := gen(2,3)

	c1 := sq(in)
	c2 := sq(in)

	for n := range merge(c1, c2) {
		fmt.Println(n)
	}
}
