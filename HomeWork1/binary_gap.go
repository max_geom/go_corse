package HomeWork1

/*
	Функция возвращает максимальное количество нулей
	в целочисленном числе преобразованного в двоичный формат
 */
func Solution(N int) int {
	LengthSequenceZero := 0
	maxSequenceLength := 0
	for N > 0 {
		if N % 2 == 1 {
			LengthSequenceZero = 0
		}else{
			LengthSequenceZero += 1
		}
		if LengthSequenceZero > maxSequenceLength {
			maxSequenceLength = LengthSequenceZero
		}
		N >>= 1
	}
	return maxSequenceLength
}
