package main

import (
	"fmt"
	"math/big"
)

/*
	Функция возвращает срез из простых чисел.
	Колисество чисел определяется в аргументе функции
 */
func Eratosthenes(countElements int) []int {
	currentCountElements := 0
	var resultSlice []int
	for i := 2; currentCountElements < countElements; i++ {
		if big.NewInt(int64(i)).ProbablyPrime(0) {
			resultSlice = append(resultSlice, i*i)
			currentCountElements += 1
		}
	}
	return resultSlice
}

func main() {
	var inputValue int
	fmt.Print("Количество простых чисел: ")
	_, err := fmt.Scan(&inputValue)
	if err != nil {
		fmt.Println("Ошибка: Невозможно конвертировать строку в число")
		return
	}
	fmt.Println(Eratosthenes(inputValue))
}
