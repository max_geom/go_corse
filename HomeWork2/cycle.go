package main

import (
	"fmt"
)

const (
	MaxLengthList = 7
)

/*
	Функция круговой очереди.
	В качестве параметрой передаётся адрес на slice и сдвиг shift.
	Функция создаёт два среза, которые меняются местами.
 */
func RoundRobin(array *[]int, shift int) {
	shiftTo := shift % MaxLengthList
	lastElements := (*array)[(MaxLengthList - shiftTo):MaxLengthList]
	firstElements := (*array)[0:(MaxLengthList - shiftTo)]
	*array = make([]int, 0 , cap(*array))
	*array = append(*array, lastElements...)
	*array = append(*array, firstElements...)
}

/*
	Программа производит сдвиг массива на N элементов по кругу.
	Программа реализует круговую очередь
 */
func main() {
	var array = []int{1,2,3,4,5,6,7}
	var shift int
	fmt.Println(array)
	fmt.Print("Введите число на которое хотите сдивнуть массив: ")
	_, err := fmt.Scan(&shift)
	if err != nil {
		fmt.Println(err)
		return
	}
	if shift != 0 {
		RoundRobin(&array, shift)
	}
	fmt.Println(array)
}
