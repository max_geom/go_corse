package main

import (
	"fmt"
	"math/rand"
)

const (
	CountElements = 100
)


/*
	Программа создаёт массив случайных чисел. Числа записываются в формате ключ
	и количество.
	Результат выполнения программы: Вывод уникальных значений.
 */
func main() {
	hashMapElements := make(map[int]int)
	for i := 0; i < CountElements; i++ {
		hashMapElements[rand.Intn(CountElements)] += 1
	}
	fmt.Println("Уникальные элементы:")
	for key, value := range hashMapElements {
		if value == 1 {
			fmt.Printf("%d - (Количество: %d)\n", key, value)
		}
	}
}
